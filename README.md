# tdd-example

TDD example project.

Как начать работу:

0. Чтобы попробовать работу с проектом, сделайте себе форк репозитория.

![](https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_fork_button.png)

0.1 Если хотите сделать отдельный проект, на основе этого - скачайте архив с файлами и добавьте в свой пустой репозиторий.

![](https://i.stack.imgur.com/OtFQq.png)

1. Если вы хотите работать с проектом в PyCharm:

- Клонировать репозиторий на машину:
```
git clone https://gitlab.com/volodink/tdd-example
```
- Перейти в папку с проектом
```
cd tdd-example
```
- Открыть проект в PyCharm**
```
pycharm
```

2.  Если вы хотите работать с проектом из коммандной строки:

- Клонировать репозиторий на машину:
```
git clone https://gitlab.com/volodink/tdd-example
```
- Перейти в папку с проектом
```
cd tdd-example
```
- Создать виртуальное окружение*
```
python -m venv .venv
```
- Активировать виртуальное окружение
```
source .venv/bin/activate
```
- Установить зависимости 
```
pip install -r requirements.txt
```
- Установить локальные пакеты в режиме редактирования
```
pip install -e .
```
- Запустить тестирование
```
pytest
```


\* Убедитесь, что используемый интерпретатор Python версии не ниже 3.7.х

\*\* Если сгенерированы и достапны в системном пути PATH скрипты апуска из коммандрой строки продуктов JetBrains. 
